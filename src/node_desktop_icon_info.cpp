#include <nan.h>
#include <gtk/gtk.h>

using namespace v8;

GtkIconTheme *iconTheme;


NAN_METHOD(GetIcon) {
    if (info.Length() > 0) {
        if (info[0]->IsString()) {
            String::Utf8Value str(info[0]);
            std::string parameter = (const char*)(*str);

            GdkPixbuf *icon;
            if (access(parameter.c_str(), F_OK) != -1) {
                icon = gdk_pixbuf_new_from_file_at_size(parameter.c_str(), info[1]->NumberValue(), info[1]->NumberValue(), NULL);
            } else {
                icon = gtk_icon_theme_load_icon(iconTheme, parameter.c_str(), info[1]->NumberValue(), GTK_ICON_LOOKUP_FORCE_SIZE, NULL);
            }

            if (icon == NULL) {
                icon = gtk_icon_theme_load_icon(iconTheme, "applications-other", info[1]->NumberValue(), GTK_ICON_LOOKUP_FORCE_SIZE, NULL);
            }
            gchar *iconbuf;
            gsize iconbufsize;
            gdk_pixbuf_save_to_buffer(icon, &iconbuf, &iconbufsize, "png", NULL, NULL);
            gchar *base64png = g_base64_encode((const guchar *) iconbuf, iconbufsize);

            Local<Value> returnValue = Nan::New(base64png).ToLocalChecked();

            info.GetReturnValue().Set(returnValue);
        }
    }
}


NAN_MODULE_INIT(RegisterModule) {
    char **newargv;
    gtk_init(0, &newargv);
    iconTheme = gtk_icon_theme_get_default();
    NAN_EXPORT(target, GetIcon);
}

NODE_MODULE(desktop_icon_info, RegisterModule);