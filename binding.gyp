{
    "variables": {
        "pkg-config": "pkg-config"
    },
    "targets": [
        {
            "target_name": "node-desktop-icon-info",
            "sources": [ "src/node_desktop_icon_info.cpp" ],
            "include_dirs": [
                "<!(node -e \"require('nan')\")"
            ],
            "cflags": [
                '<!@(<(pkg-config) --cflags gtk+-3.0)'
            ],
            "ldflags": [
                '<!@(<(pkg-config) --libs-only-L --libs-only-other gtk+-3.0)'
            ],
            "libraries": [
                '<!@(<(pkg-config) --libs-only-l gtk+-3.0)'
            ]
        }
    ]
}